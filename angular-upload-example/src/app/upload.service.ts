import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from  '@angular/common/http';  
import { map } from  'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UploadService { 
  SERVER_URL: string = "http://localhost:8081/uploadFile";  
  DOWNLOD: string = "http://localhost:8081/downloadFile";  
  constructor(private httpClient: HttpClient) { }
  
  public upload(formData) {

    return this.httpClient.post<any>(this.SERVER_URL, formData, {  
        reportProgress: true,  
        observe: 'events'  
      });  
  }

  public download(){
    return this.httpClient.get<any>(this.DOWNLOD+"/1.png",)
  }

}